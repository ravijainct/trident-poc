# Freedom Fare

Read "AIR_OTHER" international messages from air topic and prepare FreedomFareModel and push records to BQ.

## Build
```shell script
sudo gradle clean fatJar
```

## Deploy PROD
```shell script
gsutil cp freedom-fare-storm-1.0-SNAPSHOT.jar gs://data-jars
gsutil cp gs://data-jars/freedom-fare-storm-1.0-SNAPSHOT.jar ./
storm jar freedom-fare-storm-1.0-SNAPSHOT.jar com.cleartrip.analytics.bigquery.MainTopology prod
```

## Deploy QA
```shell script
scp build/libs/freedom-fare-storm-1.0-SNAPSHOT.jar root@172.17.32.61:/root/installation/topology
storm jar /root/installation/topology/freedom-fare-storm-1.0-SNAPSHOT.jar com.cleartrip.analytics.bigquery.MainTopology dev
```

