package com.cleartrip.analytics.bigquery.bolt;


import com.cleartrip.analytics.Air;
import com.cleartrip.analytics.bigquery.model.AirNRModel;
import com.google.gson.Gson;
import com.google.protobuf.InvalidProtocolBufferException;
import org.apache.storm.trident.operation.BaseFunction;
import org.apache.storm.trident.operation.TridentCollector;
import org.apache.storm.trident.tuple.TridentTuple;
import org.apache.storm.tuple.Values;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TridentBolt extends BaseFunction {

    static Logger log = LoggerFactory.getLogger(TridentBolt.class);

    private static Gson gson = new Gson();

    @Override
    public void execute(TridentTuple tuple, TridentCollector collector) {
        log.debug("Inside execute method");
        Air.AirMessage message;
        try{
            AirNRModel airNRModel = new AirNRModel();
            long msgSize = tuple.getBinary(0).length;
            message = Air.AirMessage.parseFrom((tuple.getBinary(0)));
            if (message==null || msgSize > 20971520)  return;
            if (message.getHeader().hasItineraryId() && message.getHeader().getItineraryId() != null) {
                airNRModel.setEventType("TRIDENT_POC");
                airNRModel.setItineraryId(message.getHeader().getItineraryId());
                airNRModel.setChannel(message.getHeader().getChannel().name());
                airNRModel.setDomain(message.getHeader().getDomain());
                log.debug("nr model: {}", airNRModel.toString());
                log.info("Success execute method");
            }
            collector.emit(new Values(gson.toJson(airNRModel)));
        }catch (InvalidProtocolBufferException e) {
            log.error("Error: ", e);
            collector.reportError(e);
        }
    }
}
