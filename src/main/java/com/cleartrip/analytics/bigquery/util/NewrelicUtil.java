package com.cleartrip.analytics.bigquery.util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.GzipCompressingEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NewrelicUtil {
	
	private static final Logger log = LoggerFactory.getLogger(NewrelicUtil.class);
	
	public static String sendPostRequest(String body) {
		log.info("Sending HTTP Post Request");
		String content = null;
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpPost httpPost = new HttpPost("http://insights-collector.newrelic.com/v1/accounts/1835861/events");
		try {
			httpPost.addHeader("x-insert-key","IRXmkwBl64QIsb8KF4Vf6KqSZmjra7lo");
			httpPost.addHeader("content-type","application/gzip");
			httpPost.addHeader("cache-control", "no-cache");
			StringEntity stringEntity = new StringEntity(body);
			GzipCompressingEntity gzipENtity = new GzipCompressingEntity(stringEntity);
	        httpPost.setEntity(gzipENtity);
	        log.info("Sending Request to New Relic with Body : {}",body);
			HttpResponse response = httpClient.execute(httpPost);
			HttpEntity respEntity = response.getEntity();
			if (respEntity != null) {
				 content = EntityUtils.toString(respEntity);
				 log.info("Response: {}", content);
			}
		} catch (Exception e){
			log.error("Exception pushing to NR is: ",e);
		}finally {
			log.info("Completed HTTP Post Request");
		}
		return content;
	}
}
