package com.cleartrip.analytics.bigquery;

import com.cleartrip.analytics.bigquery.bolt.TridentBolt;
import com.cleartrip.analytics.bigquery.state.AirNRStateFactory;
import com.cleartrip.analytics.bigquery.state.AirNRStateUpdater;
import com.cleartrip.analytics.bigquery.state.PartitionAggregate;
import org.apache.storm.Config;
import org.apache.storm.StormSubmitter;
import org.apache.storm.generated.AlreadyAliveException;
import org.apache.storm.generated.AuthorizationException;
import org.apache.storm.generated.InvalidTopologyException;
import org.apache.storm.kafka.BrokerHosts;
import org.apache.storm.kafka.ZkHosts;
import org.apache.storm.kafka.trident.OpaqueTridentKafkaSpout;
import org.apache.storm.kafka.trident.TridentKafkaConfig;
import org.apache.storm.trident.TridentTopology;
import org.apache.storm.trident.spout.IOpaquePartitionedTridentSpout;
import org.apache.storm.trident.state.StateFactory;
import org.apache.storm.trident.testing.MemoryMapState;
import org.apache.storm.tuple.Fields;

import static kafka.api.OffsetRequest.EarliestTime;

public class MainTopology {

    public static void main(String[] args) throws InvalidTopologyException, AuthorizationException, AlreadyAliveException {

        BrokerHosts zkHosts = new ZkHosts("zoo1.cleartrip.com:2181", "/brokers");

        TridentKafkaConfig tridentKafkaConfig = new TridentKafkaConfig(zkHosts,"air","trident-air-1");
        tridentKafkaConfig.ignoreZkOffsets = true;
        tridentKafkaConfig.startOffsetTime = EarliestTime();
        tridentKafkaConfig.fetchSizeBytes = 2097152;

        IOpaquePartitionedTridentSpout partitionedTridentSpout = new OpaqueTridentKafkaSpout(tridentKafkaConfig);

        StateFactory stateFactory = new AirNRStateFactory();

        TridentTopology topology = new TridentTopology();
        topology.newStream("kafka-spout", partitionedTridentSpout)
                .parallelismHint(1)
                .each(new Fields("bytes"), new TridentBolt(), new Fields("airNRModel"))
                .parallelismHint(1)
                .shuffle()
                .partitionPersist(
                        new AirNRStateFactory(),
                        new Fields("airNRModel"),
                        new AirNRStateUpdater());

        Config conf = new Config();
        conf.setDebug(false);
        conf.setNumWorkers(1);
        conf.setMaxSpoutPending(100);

        StormSubmitter.submitTopologyWithProgressBar("trident-poc", conf, topology.build());
    }
}