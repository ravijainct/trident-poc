package com.cleartrip.analytics.bigquery.state;

import org.apache.storm.task.IMetricsContext;
import org.apache.storm.trident.state.State;
import org.apache.storm.trident.state.StateFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class AirNRStateFactory implements StateFactory {

    static Logger log = LoggerFactory.getLogger(AirNRStateFactory.class);

    public AirNRStateFactory() {
        log.info("State Factory Constructor");
    }

    @Override
    public State makeState(Map conf, IMetricsContext metrics, int partitionIndex, int numPartitions) {
        log.info("StateFactory\nPartitionIndex: {}\nNumPartitions: {}", partitionIndex, numPartitions);
        return new AirNRState();
    }
}
