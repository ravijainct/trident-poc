package com.cleartrip.analytics.bigquery.state;

import com.cleartrip.analytics.bigquery.util.NewrelicUtil;
import org.apache.storm.trident.state.State;
import org.apache.storm.trident.tuple.TridentTuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

public class AirNRState implements State {

    static Logger log = LoggerFactory.getLogger(AirNRState.class);

    protected AirNRState() {
        log.info("State Constructor");
    }

    @Override
    public void beginCommit(Long txid) {
        log.info("Inside begin comit, txid: {}", txid);
    }

    @Override
    public void commit(Long txid) {
        log.info("Inside comit, txid: {}", txid);
    }

    public void updateState(List<TridentTuple> tuples) {
        String nrRequest = buildNRRequest(tuples);
        NewrelicUtil.sendPostRequest(nrRequest);
        log.info("Updated State Successfully");
    }

    private String buildNRRequest(List<TridentTuple> tuples) {
        log.info("Batch Size: {}", tuples.size());
        String nrRequest = tuples.parallelStream().flatMap(List::stream).collect(Collectors.toList()).toString();
        log.info("NR Request: {}", nrRequest);
        return nrRequest;
    }
}
