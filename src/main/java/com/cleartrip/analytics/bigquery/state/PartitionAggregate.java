package com.cleartrip.analytics.bigquery.state;

import org.apache.storm.trident.operation.CombinerAggregator;
import org.apache.storm.trident.tuple.TridentTuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PartitionAggregate implements CombinerAggregator {

    static Logger log = LoggerFactory.getLogger(PartitionAggregate.class);

    @Override
    public Object init(TridentTuple tuple) {
        log.info("Inside init method; tuple {}", tuple.toString());
        return null;
    }

    @Override
    public Object combine(Object val1, Object val2) {
        log.info("Object 1: {}\nObject 2: {}", val1, val2);
        return null;
    }

    @Override
    public Object zero() {
        log.info("Inside zero");
        return null;
    }
}
