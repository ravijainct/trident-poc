package com.cleartrip.analytics.bigquery.state;

import org.apache.storm.trident.operation.TridentCollector;
import org.apache.storm.trident.state.BaseStateUpdater;
import org.apache.storm.trident.tuple.TridentTuple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class AirNRStateUpdater extends BaseStateUpdater<AirNRState> {

    static Logger log = LoggerFactory.getLogger(AirNRStateUpdater.class);

    public AirNRStateUpdater() {
        log.info("State Update Constructor");
    }

    @Override
    public void updateState(AirNRState state, List<TridentTuple> tuples, TridentCollector collector) {
        log.info("Update State ...");
        state.updateState(tuples);
    }
}
