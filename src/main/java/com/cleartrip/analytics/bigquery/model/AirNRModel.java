package com.cleartrip.analytics.bigquery.model;

import lombok.Data;

@Data
public class AirNRModel {
    String eventType;
    String itineraryId;
    String channel;
    String domain;
}
